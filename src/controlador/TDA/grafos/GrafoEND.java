package Controlador.TDA.grafos;

import Controlador.Exceptions.PosicionException;
import Controlador.Exceptions.VerticeException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ronald Cuenca
 */
public class GrafoEND<E> extends GrafoDE<E> {

    public GrafoEND(Integer numV, Class clazz) {
        super(numV, clazz);
    }

    @Override
    public void insertarArista(Integer i, Integer j, Double peso) throws VerticeException {
        if (i > 0 && j > 0 &&i <= numV && j <= numV) {
            Object[] existe = existeArista(i, j);
            if (!((Boolean) existe[0])) {
                numA++;
                listaAdyacente[i].insertarCabecera(new Adyacencia(j, peso));
                listaAdyacente[j].insertarCabecera(new Adyacencia(i, peso));
                
            }
        } else {
            throw new VerticeException("Algun vertice ingresado no existe");
        }
    }
    
    public void insertarArista2(Integer i, Integer j, Double peso) throws VerticeException {
        if (i > 0 && j > 0 &&i < numV && j < numV) {
            Object[] existe = existeArista(i, j);
            if (!((Boolean) existe[0])) {
                numA++;
                try {
                    //                listaAdyacente[i].insertarCabecera(new Adyacencia(j, peso));
                    listaAdyacente[i].modificarDato(j, new Adyacencia(j, peso));
                    listaAdyacente[j].modificarDato(i, new Adyacencia(i, peso));
                } catch (PosicionException ex) {
                    System.out.println(ex);
                }
//                listaAdyacente[j].insertarCabecera(new Adyacencia(i, peso));
                
            }
        } else {
            throw new VerticeException("Algun vertice ingresado no existe");
        }
    }

    @Override
    public void insertarAristaE(E i, E j, Double peso) throws Exception {
        insertarArista(obtenerCodigo(i), obtenerCodigo(j), peso);
        insertarArista(obtenerCodigo(j), obtenerCodigo(i), peso);
    }
   
}
