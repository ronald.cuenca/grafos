package Controlador.TDA.grafos;

import Controlador.Exceptions.PosicionException;
import Controlador.Exceptions.VerticeException;
import controlador.lista.ListaEnlazada;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Ronald Cuenca
 */
public class GrafoD extends Grafo {

    protected Integer numV;
    protected Integer numA;
    protected ListaEnlazada<Adyacencia> listaAdyacente[];

    public GrafoD(Integer numV) {
        this.numV = numV;
        this.numA = 0;
        listaAdyacente = new ListaEnlazada[numV+1];
        for (int i = 0; i <= this.numV; i++) {
            listaAdyacente[i] = new ListaEnlazada<>();
        }
    }

    @Override
    public Integer numVertices() {
        return this.numV;
    }

    @Override
    public Integer numAristas() {
        return this.numA;
    }

    /**
     * Permite verificar si existe una conexion entre aristas
     *
     * @param i vertice inicial
     * @param f vertice final
     * @return arreglo de objetos: en la posicion 0 regresa un boolean y en la 1
     * el peso
     * @throws VerticeException
     */
    @Override
    public Object[] existeArista(Integer i, Integer f) throws VerticeException {
        Object[] resultado = {Boolean.FALSE, Double.NaN};
        if (i > 0 && f > 0 && i <= numV && f <= numV) {
            ListaEnlazada<Adyacencia> lista = listaAdyacente[i];
            for (int j = 0; j < lista.getSize(); j++) {
                try {
                    Adyacencia aux = lista.obtenerDato(j);
                    if (aux.getDestino().intValue() == f.intValue()) {
                        resultado[0] = Boolean.TRUE;
                        resultado[1] = aux.getPeso();
                        break;
                    }
                } catch (PosicionException ex) {

                }
            }
        } else {
            throw new VerticeException("Algun vertice ingresado no existe");
        }
        return resultado;
    }

    @Override
    public Double pesoArista(Integer i, Integer f) throws VerticeException {
        Double peso = Double.NaN;
        Object[] existe = existeArista(i, f);
        if (((Boolean) existe[0])) {
            peso = (Double) existe[i];
        }
        return peso;
    }

    @Override
    public void insertarArista(Integer i, Integer j) throws VerticeException {
        insertarArista(i, j, Double.NaN);
    }

    @Override
    public void insertarArista(Integer i, Integer j, Double peso) throws VerticeException {
        if (i > 0 && j > 0 &&i <= numV && j <= numV) {
            Object[] existe = existeArista(i, j);
            if (!((Boolean) existe[0])) {
                numA++;
                try {
                    listaAdyacente[i].modificarDato(j, new Adyacencia(j, peso));
                } catch (PosicionException ex) {
                    System.out.println(ex);
                }
            }
        } else {
            throw new VerticeException("Algun vertice ingresado no existe");
        }

    }
    
    public void insertarArista2(Integer i, Integer j, Double peso) throws VerticeException {
        if (i > 0 && j > 0 &&i <= numV && j <= numV) {
            Object[] existe = existeArista(i, j);
            if (!((Boolean) existe[0])) {
                numA++;
                    listaAdyacente[i].insertarCabecera(new Adyacencia(j, peso));

        } else {
            throw new VerticeException("Algun vertice ingresado no existe");
        }
        }
    }

    @Override
    public ListaEnlazada<Adyacencia> adyacente(Integer i) throws VerticeException {
        return listaAdyacente[i];
    }
    
    public void llenarGrafo() throws PosicionException{
        for (int i = 0; i <= listaAdyacente.length-1; i++) {
            for (int j = 0; j <= listaAdyacente.length-1; j++) {
                listaAdyacente[i].insertarCabecera(new Adyacencia(1, 1000000.0));
            }
        }
    }
    
    public static ListaEnlazada<Adyacencia>[] caminoCortoV2(ListaEnlazada<Adyacencia> adj[], ListaEnlazada<Adyacencia> path[]) throws PosicionException {
        int n = adj.length-1;
        ListaEnlazada<Adyacencia> ListaAux[] = new ListaEnlazada[n];
        System.out.println("dAD: "+adj.length);
        auxiliar(ListaAux, adj);
        for (int k = 0; k < n; k++)
            for (int i = 0; i < n; i++) 
                for (int j = 0; j < n; j++) 
                    
                    if ((ListaAux[i].obtenerDato(k).getPeso() + ListaAux[k].obtenerDato(j).getPeso()) < ListaAux[i].obtenerDato(j).getPeso()) {
                        ListaAux[i].modificarDato(j, new Adyacencia(1, ListaAux[i].obtenerDato(k).getPeso() + ListaAux[k].obtenerDato(j).getPeso()));
                        path[i].modificarDato(j, path[k].obtenerDato(j));
                    }
        return ListaAux;
    }

    public static void auxiliar(ListaEnlazada<Adyacencia> a[], ListaEnlazada<Adyacencia> b[]) throws PosicionException {
        for (int i = 0; i < a.length; i++) {
            a[i] = new ListaEnlazada<>();
        }
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                a[i].insertar(b[i].obtenerDato(j), j);
            }
        }
    }

    public String recorridoFloyd(Integer VInicio, Integer VFinal) throws PosicionException, VerticeException {
        ListaEnlazada<Adyacencia> aux[] = listaAdyacente;
        ListaEnlazada<Adyacencia> listaAdyacente2[] = new ListaEnlazada[numV];
        for (int i = 0; i < numV; i++) {
            listaAdyacente2[i] = new ListaEnlazada<>();
        }
        for (int i = 0; i < numV; i++) {
            for (int j = 0; j < numV; j++) {
                    if (aux[i].obtenerDato(j).getPeso() == 1000000) {
                    listaAdyacente2[i].insertar(new Adyacencia(null, -1.0), j);
                } else {
                    listaAdyacente2[i].insertar(new Adyacencia(null, Double.valueOf(i)), j);
                }
            }
        }
        for (int k = 0; k < numV; k++) {
            listaAdyacente2[k].modificarDato(k, new Adyacencia(null, Double.valueOf(k)));
        }
        System.out.println("Datito: "+numV);
        caminoCortoV2(aux, listaAdyacente2);
        
        System.out.println("Ruta más corta de un vértice a otro (0 a 4)");
        System.out.print("Vértice inicial: ");
        int start = VInicio;
        System.out.print("Vértice final: ");
        int end = VFinal;
        String myPath = end + "";
        System.out.println();
        System.out.println("  0 1 2 3 4");
        System.out.println("  ---------");
        for (int i = 0; i < numV; i++) {
            System.out.print(i + "|");
            for (int j = 0; j < numV; j++) {
                System.out.print(listaAdyacente2[i].obtenerDato(j).getPeso() + " ");
            }
            System.out.println();
        }
        
        while (listaAdyacente2[start].obtenerDato(end).getPeso() != start) {
            myPath = listaAdyacente2[start].obtenerDato(end).getPeso() + " -> " + myPath;
            end = (int) Math.round(listaAdyacente2[start].obtenerDato(end).getPeso());
            
        }
        myPath = start + " -> " + myPath;
        System.out.println("Esta es la ruta: " + myPath);
        return myPath;
    }
}
