package Vista.Modelo;

import Controlador.Exceptions.VerticeException;
import Controlador.TDA.grafos.GrafoD;
import Controlador.TDA.grafos.GrafoEND;
import modelo.persona;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Ronald Cuenca
 */
public class ModeloTablaPersona extends AbstractTableModel {

    private GrafoEND<persona> grafoEND;
    private String[] columnas;

    public GrafoEND getGrafoEND() {
        return grafoEND;
    }

    public void setGrafoEND(GrafoEND grafoEND) {
        this.grafoEND = grafoEND;
    }

    public String[] getColumnas() {
        return columnas;
    }

    public void setColumnas(String[] columnas) {
        this.columnas = columnas;
    }

    @Override
    public int getRowCount() {
        return grafoEND.numVertices();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Nro";
            case 1:
                return "Nombres";
            case 2:
                return "Tipo";
            case 3:
                return "Ubicacion";
            default:
                return null;
        }
    }

    @Override
    public Object getValueAt(int arg0, int arg1) {
        try {
            persona p = grafoEND.obtenerEtiqueta(arg0+1);
            switch (arg1) {
                case 0:
                    return (arg0+1);
                case 1:
                    return p.getNombre();
                case 2:
                    return p.getApellido().toString();
                case 3:
                    return p.getCedula();
                default:
                    return null;
            }
        } catch (Exception ex) {
            System.out.println("ERROR: " + ex);
            return null;
        }
    }

}
