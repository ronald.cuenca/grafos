package vista.Modelo;

import Controlador.Exceptions.VerticeException;
import Controlador.TDA.grafos.GrafoD;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Ronald Cuenca
 */
public class ModeloTablaGrafoND extends AbstractTableModel{
    
    private GrafoD grafoD;
    private String[] columnas;

    private String [] generarColumna(){
        columnas = new String [grafoD.numVertices()+1];
        columnas[0] = "/";
        for (int i = 0; i < columnas.length; i++) {
            columnas[i] = String.valueOf(i);
        }
        return columnas;
    }

    public GrafoD getGrafoD() {
        return grafoD;
    }

    public void setGrafoD(GrafoD grafoD) {
        this.grafoD = grafoD;
        generarColumna();
    }
    
    @Override
    public int getRowCount() {
        return grafoD.numVertices();
    }

    @Override
    public int getColumnCount() {
        return grafoD.numVertices()+1;
    }

    @Override
    public String getColumnName(int column) {
        return columnas[column];
    }

    @Override
    public Object getValueAt(int arg0, int arg1) {
        if (arg1==0) {
            return columnas[arg0+1];
        }else{
            try {
                Object [] aux = grafoD.existeArista((arg0+1), arg1);
                if ((Boolean) aux[0]) {
                    return aux[1];
                }else{
                    return "----------";
                }
            } catch (VerticeException ex) {
                System.out.println("ERROR EN LA TABLA " + ex);
                return "";
            }
        }
    }
    
}
